import axios from 'axios'
import router from '../router'
import store from './store'

export default axios.create({
  baseURL: 'http://localhost:3000',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

api.interceptors.request.use(
  res => {
    const token = localStorage.getItem('token');
    if (token) {
      res.headers.common['Authorization'] = 'Bearer' + token;
    }
    return res;
  },
  err => {
    return Promise.reject(err);
  }
);
api.interceptors.res.use(
  res => {
    if (res.status === 200 || res.status === 201) {
      return Promise.resolve(res);
    } else {
      return Promise.reject(res);
    }
  },
err => {
    if (err.res.status) {
      switch (err.res.status) {
        case 400:
         console.log('Status 400')
         //do something
          break;
      
        case 401:
          console.log('Session expired')
          store.dispatch('auth/logout')
          router.replace({
            path: '/login',
            query: {redirect: router.currentRoute.fullPath}
          })
          break;
        case 403:
          router.replace({
            path: "/login",
            query: { redirect: router.currentRoute.fullPath }
          });
           break;
        case 404:
          alert('Status 404');
          break;
        case 502:
         setTimeout(() => {
            router.replace({
              path: "/login",
              query: {
                redirect: router.currentRoute.fullPath
              }
            });
          }, 1000);
      }
      return Promise.reject(err.res);
    }
  }
)

export default api